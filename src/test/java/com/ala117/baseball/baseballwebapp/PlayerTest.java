package com.ala117.baseball.baseballwebapp;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PlayerTest {

	static final String NAME = "Cal Ripken";
	static final int NUMBER = 8;
	static final Position POSITION = Position.INFIELD;
	
	private Player player;
	
	@Before
	public void initialize() {
		player = new Player(NAME, NUMBER, POSITION);
	}
	
	@Test
	public void testGetName() {
		assertEquals(NAME, player.getName());
	}

	@Test
	public void testGetPosition() {
		assertEquals(POSITION.name(), player.getPosition().name());
	}

	@Test
	public void testGetNumber() {
		assertEquals(NUMBER, player.getNumber());
	}

	@Test
	public void testSetName() {
		String newName = "Calvin Ripken";
		player.setName(newName);
		assertEquals(newName, player.getName());
	}

	@Test
	public void testSetPosition() {
		player.setPosition(Position.PITCHER);
		assertEquals(Position.PITCHER, player.getPosition());
	}

	@Test
	public void testSetNumber() {
		player.setNumber(88);
		assertEquals(88, player.getNumber());
	}

}
