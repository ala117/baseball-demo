package com.ala117.baseball.baseballwebapp;

import java.util.List;

import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

@RestController
public class PlayerController {
	private final PlayerRepository repository;

	PlayerController(PlayerRepository repository) {
		this.repository = repository;
	}

	// Aggregate root

	@GetMapping("/players")
	List<Player> all() {
		return repository.findAll();
	}

	@PostMapping("/players")
	Player newPlayer(@RequestBody Player newPlayer) {
		return repository.save(newPlayer);
	}

	// Single item

	@GetMapping("/players/{id}")
	Resource<Player> one(@PathVariable Long id) throws PlayerNotFoundException {

		Player player = repository.findById(id)
			.orElseThrow(() -> new PlayerNotFoundException(id));
		
		return new Resource<>(player,
				linkTo(methodOn(PlayerController.class).one(id)).withSelfRel(),
				linkTo(methodOn(PlayerController.class).all()).withRel("players"));
	}

	@PutMapping("/players/{id}")
	Player replacePlayer(@RequestBody Player newPlayer, @PathVariable Long id) {

		return repository.findById(id)
			.map(player -> {
				player.setName(newPlayer.getName());
				player.setNumber(newPlayer.getNumber());
				player.setPosition(newPlayer.getPosition());
				return repository.save(player);
			})
			.orElseGet(() -> {
				newPlayer.setId(id);
				return repository.save(newPlayer);
			});
	}

	@DeleteMapping("/players/{id}")
	void deletePlayer(@PathVariable Long id) {
		repository.deleteById(id);
	}
}
