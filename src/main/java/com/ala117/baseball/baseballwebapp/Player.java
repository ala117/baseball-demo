package com.ala117.baseball.baseballwebapp;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@Entity
public class Player {

    private @Id @GeneratedValue Long id;
    private String name;
    private Position position;
    private @Min(0) @Max(99) int number;

    Player() {
    	name = "";
    	position = null;
    	number = 0;
    }
    
    Player(String name, int number, Position position) {
            this.name = name;
            this.number = number;
            this.position = position;
    }
}
