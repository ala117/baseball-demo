package com.ala117.baseball.baseballwebapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BaseballWebappApplication {

	public static void main(String[] args) {
		SpringApplication.run(BaseballWebappApplication.class, args);
	}
}
