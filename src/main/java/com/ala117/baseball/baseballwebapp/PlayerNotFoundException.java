package com.ala117.baseball.baseballwebapp;

public class PlayerNotFoundException extends Exception {

	private static final long serialVersionUID = 1196L;

	public PlayerNotFoundException(long id) { 
        super(String.valueOf(id)); 
    } 
}
