package com.ala117.baseball.baseballwebapp;

public enum Position {
    PITCHER, CATCHER, INFIELD, OUTFIELD
}
