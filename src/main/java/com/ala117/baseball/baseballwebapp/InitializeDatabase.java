package com.ala117.baseball.baseballwebapp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InitializeDatabase {
	private Logger logger = LoggerFactory.getLogger(InitializeDatabase.class);
	
	@Bean
	CommandLineRunner initDatabase(PlayerRepository repository) {
		return args -> {
			logger.info("Preloading " + repository.save(new Player("Justin Turner", 10, Position.INFIELD)));
			logger.info("Preloading " + repository.save(new Player("Steve Pearce", 25, Position.INFIELD)));
			logger.info("Preloading " + repository.save(new Player("Matt Barnes", 32, Position.PITCHER)));
			logger.info("Preloading " + repository.save(new Player("Ryan Brasier", 70, Position.PITCHER)));
			logger.info("Preloading " + repository.save(new Player("William Cuevas", 67, Position.PITCHER)));
			logger.info("Preloading " + repository.save(new Player("Justin Haley", 65, Position.PITCHER)));
			logger.info("Preloading " + repository.save(new Player("Heath Hembree", 37, Position.PITCHER)));
			logger.info("Preloading " + repository.save(new Player("Brian Johnson", 61, Position.PITCHER)));
			logger.info("Preloading " + repository.save(new Player("Rick Porcello", 22, Position.PITCHER)));
			logger.info("Preloading " + repository.save(new Player("Bobby Poyner", 66, Position.PITCHER)));
			logger.info("Preloading " + repository.save(new Player("David Price", 24, Position.PITCHER)));
			logger.info("Preloading " + repository.save(new Player("Eduardo Rodriguez", 57, Position.PITCHER)));
			logger.info("Preloading " + repository.save(new Player("Chris Sale", 41, Position.PITCHER)));
			logger.info("Preloading " + repository.save(new Player("Robby Scott", 63, Position.PITCHER)));
			logger.info("Preloading " + repository.save(new Player("Tyler Thornburg", 47, Position.PITCHER)));
			logger.info("Preloading " + repository.save(new Player("Hector Velazquez", 76, Position.PITCHER)));
			logger.info("Preloading " + repository.save(new Player("Marcus Walden", 64, Position.PITCHER)));
			logger.info("Preloading " + repository.save(new Player("Brandon Workman", 44, Position.PITCHER)));
			logger.info("Preloading " + repository.save(new Player("Steven Wright", 35, Position.PITCHER)));
			logger.info("Preloading " + repository.save(new Player("Sandy Leon", 3, Position.CATCHER)));
			logger.info("Preloading " + repository.save(new Player("Blake Swihart", 23, Position.CATCHER)));
			logger.info("Preloading " + repository.save(new Player("Christian Vazquez", 7, Position.CATCHER)));
			logger.info("Preloading " + repository.save(new Player("Xander Bogaerts", 2, Position.INFIELD)));
			logger.info("Preloading " + repository.save(new Player("Rafael Devers", 11, Position.INFIELD)));
			logger.info("Preloading " + repository.save(new Player("Brock Holt", 12, Position.INFIELD)));
			logger.info("Preloading " + repository.save(new Player("Tzu-Wei Lin", 30, Position.INFIELD)));
			logger.info("Preloading " + repository.save(new Player("Mitch Moreland", 18, Position.INFIELD)));
			logger.info("Preloading " + repository.save(new Player("Eduardo Nunez", 36, Position.INFIELD)));
			logger.info("Preloading " + repository.save(new Player("Tony Renda", 38, Position.INFIELD)));
			logger.info("Preloading " + repository.save(new Player("Sam Travis", 59, Position.INFIELD)));
			logger.info("Preloading " + repository.save(new Player("Andrew Benintendi", 16, Position.OUTFIELD)));
			logger.info("Preloading " + repository.save(new Player("Mookie Betts", 50, Position.OUTFIELD)));
			logger.info("Preloading " + repository.save(new Player("Jackie Bradley", 19, Position.OUTFIELD)));
			logger.info("Preloading " + repository.save(new Player("J.D. Martinez", 28, Position.OUTFIELD)));
		};
	}
}
